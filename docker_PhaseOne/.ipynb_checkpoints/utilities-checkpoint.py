import os
from tqdm import tqdm
import numpy as np

from stardist.models import StarDist2D 
from skimage.io import imread, imsave, imshow
from skimage.filters import median
from skimage.morphology import disk
import cv2
from csbdeep.utils import normalize

import btrack
from skimage import exposure
import json
import tifffile

def get_files(path):
    files = [f for f in os.listdir(path) if f.endswith(".tif")]
    return files

def preprocess(img):
    return normalize(cv2.medianBlur(img,5))

def segment(path_in):
    files = get_files(path_in)
    seg_dir = ""
    
    model = StarDist2D.from_pretrained('2D_versatile_fluo')
    count = 0
    for f in files:
        count +=1
        imgs = imread(path_in+f)
        new_imgs = np.empty_like(imgs)
        for i in tqdm(range(imgs.shape[0])):
            img = imgs[i]
            img = preprocess(img)
            lbl, _ = model.predict_instances(img)
            new_imgs[i] = lbl
        imsave(seg_dir+"seg_"+f, new_imgs)
        print("{} out of {} files binarized and saved".format(count, len(files)))

def track(path_in, path_out):
    files = get_files(path_in)
    seg_dir = ""
    
    count = 0
    for f in files:
        count +=1
        img = exposure.adjust_gamma(imread(path_in+f), 0.1)
        segmentation = imread(seg_dir+'seg_'+f)
        objects = btrack.utils.segmentation_to_objects(segmentation, properties=('area', ))
        with btrack.BayesianTracker() as tracker:
            tracker.configure_from_file('/gorbsky_data/docker_PhaseOne/tracking/cell_config.json')
            tracker.append(objects)
            tracker.volume=((0, 1200), (0, 1600), (-1e5, 1e5))
            tracker.track_interactive(step_size=100)
            tracker.optimize()
            tracker.export('/gorbsky_data/docker_PhaseOne/tracking/tracks.h5', obj_type='obj_type_1')
            tracks = tracker.tracks
        
        s = 128
        if not os.path.exists(path_out+f[:-4]+'_tracked/'):
            os.mkdir(path_out+f[:-4]+'_tracked/')
        for t in tqdm(tracks):
            track_vid = np.zeros((len(t.x),s,s))
            coords = {}
            for i in range(len(t.x)):
                x = int(t.x[i])
                y = int(t.y[i])
                x_low = x - int(s/2)
                x_low_val = max(0, x_low)
                x_high = x + int(s/2)
                x_high_val = min(1608, x_high)
                y_low = y - int(s/2)
                y_low_val = max(0, y_low)
                y_high = y + int(s/2)
                y_high_val = min(1608, y_high)
                track_vid[i,(y_low_val-y_low):(2*int(s/2)+y_high_val-y_high),(x_low_val-x_low):(2*int(s/2)+x_high_val-x_high)] = img[i,y_low_val:y_high_val,x_low_val:x_high_val]
                coords[int(t.t[i])] = (x,y)
            imsave(path_out+f[:-4]+'_tracked/btrack'+str(t.ID)+'.tif', track_vid)
            np.save(path_out+f[:-4]+'_tracked/btrack'+str(t.ID)+'.npy', coords)
        print("{} out of {} files of cells tracked and saved".format(count, len(files)))
