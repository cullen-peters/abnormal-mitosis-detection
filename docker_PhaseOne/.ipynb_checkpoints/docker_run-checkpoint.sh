#!/bin/bash

docker build -t cullenp/phase_one .
docker run --rm \
    -v /data/backup/cullen/gorbsky_data/:/gorbsky_data \
    --device /dev/nvidia0:/dev/nvidia0 \
    --device /dev/nvidiactl:/dev/nvidiactl \
    --device /dev/nvidia-caps:/dev/nvidia-caps \
    --device /dev/nvidia-modeset:/dev/nvidia-modeset \
    --device /dev/nvidia-uvm:/dev/nvidia-uvm \
    --device /dev/nvidia-uvm-tools:/dev/nvidia-uvm-tools \
    --gpus 'all,"capabilities=compute,utility"' \
    --runtime nvidia \
    --network host --privileged\
    --name phase_one \
    -it cullenp/phase_one pipeline.py /gorbsky_data/docker_PhaseOne/test_input/ /gorbsky_data/tracking_videos/
