"""
run this:
python3 pipeline.py "/gorbsky_data/input_data/" "/gorbsky_data/tracking_videos/"
"""
import sys
from utilities import segment, track

segment(sys.argv[1])
track(sys.argv[1], sys.argv[2])